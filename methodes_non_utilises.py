""" Fonction qui demande au client de choisir une ou plusieurs fiches pour les sauvegarder dans une liste et les affiche. """
""" Était dans page.py """
def ajouterFiches():

    #   Efface la liste des fiches/dossiers dans la subframeDeLaListeFiches, afin d'eviter des duplications dans l'affichage
    for widget in subframeDeLaListeFiches.winfo_children():
        widget.destroy()
    
    # Ouvrir la fenetre permettant le choix de dossier
    fiches= filedialog.askopenfilenames(initialdir="/", title="Choisir la fiche a copier")

    # Si fiches sont choisis (Bool(fiche) = Vraie), ajout à la liste des fiches/dossiers
    if bool(fiches):
        liste_des_fiches.append(fiches)
    
    # Ajouter le dossier dans la liste et l'afficher dans le subframeDeLaListeFiches
    if bool(liste_des_fiches):
        for fiche in liste_des_fiches:
            label = tk.Label(subframeDeLaListeFiches, text = fiche, bg="gray")
            label.pack()


""" Fonction qui demande au client de choisir un dossier pour les sauvegarder dans une liste et les afficher. """
""" Etait dans page.py """
def ajouterDossier():

    #   Efface la liste des fiches/dossiers dans la subframeDeLaListeFiches, afin d'eviter des duplications dans l'affichage
    for widget in subframeDeLaListeFiches.winfo_children():
        widget.destroy()
    
    # Ouvrir la fenetre permettant le choix de dossier
    dossier = filedialog.askdirectory(initialdir="/", title="Choisir le dossier a copier")

    # Si dossier est choisi (Bool(dossier) = Vraie), ajout à la liste des fiches/dossiers
    if bool(dossier):
        liste_des_fiches.append(dossier)
    
    # Ajouter le dossier dans la liste et l'afficher dans le subframeDeLaListeFiches
    if bool(liste_des_fiches):
        for fiche in liste_des_fiches:
            label = tk.Label(subframeDeLaListeFiches, text = fiche, bg="gray",)
            label.pack()

""" Fonction qui efface la liste affiché et la liste de python """
""" Etait dans page.py """
def clear():
    # Efface liste affiché
    for widget in subframeDeLaListeFiches.winfo_children():
        widget.destroy()
        # Efface la liste 
        liste_des_fiches.clear()