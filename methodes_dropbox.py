import re
import dropbox


# ~~~~~~~~~~~~~~~~~~~~ Setup de l'objet Dropbox et des utils ~~~~~~~~~~~~~~~~~~~~~~~~~~
token = "n28tOc_l7VQAAAAAAAAAASZTxqtgAWGts4sgwR3fowxCVzfSbRf2vrZmEHcwFOwf"
dbx = dropbox.Dropbox(token)
dossier_db_televersement = "/Automation"

files_to_copy  = []
# Ajouter les paths des dossiers a copier (Se trouvent dans le dossier "/Automation")
for file in dbx.files_list_folder(path = dossier_db_televersement).entries:
    files_to_copy.append(file.path_display)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

""" Methode qui retourne la liste des fiches dans le dossier <<Automation>> """
def fiches_a_copier_dans_dropbox():
    liste_des_fiches = []
    # Pour chaque fiche dans <</Automation>>, ajoute son path à la liste
    for file in dbx.files_list_folder(path = dossier_db_televersement, recursive=True).entries:
        liste_des_fiches.append(file.path_display)

    return liste_des_fiches

""" Methode qui fait le copiage des fiches selon la liste des eleves """
def copier_fiches(liste_des_eleves):
    paths = []
    temp = []
    batch_copy_paths = []

    # Sauvegarder tout les dossiers
    dossiers = dbx.files_list_folder(path="/003 - Professeurs", recursive=True).entries

    # Ajouter tout les paths des profs dans temp
    for element in dossiers:
        temp.append(element.path_display)

    # Avec les paths des professeurs, trouver le path des élèves dans la liste
    for eleve in liste_des_eleves:
        r = re.compile(f'.+?/{eleve}') # Cherche le path qui termine par le nom de l'élève
        paths += list(filter(r.match, temp))
    
    # Avec les paths des élèves, créer la liste des combinaison "to" & "from"
    # Pour chaque élève, 
    for path_dossier_eleve in paths:
        for path_fiche in files_to_copy:
            nom_de_la_fiche = path_fiche.split("/")[-1] # Je veux juste le nom de la fiche, "example/path/(nomDeLaFiche)"
            batch_copy_paths.append(dropbox.files.RelocationPath(from_path= path_fiche, 
                                                                to_path = path_dossier_eleve + "/" + nom_de_la_fiche))

    # Finalement, batch copie
    dbx.files_copy_batch_v2(batch_copy_paths, autorename=False)

"""  Methode qui fait le copiage des fiches selon la liste des professeurs"""
def copier_fiches_profs(liste_des_profs):
    paths = []
    temp = []
    batch_copy_paths = []
    liste_des_eleves = []

    # Sauvegarder tout les dossiers
    dossiers = dbx.files_list_folder(path="/003 - Professeurs", recursive=True).entries

    # Ajouter tout les paths des profs dans temp
    for element in dossiers:
        temp.append(element.path_display)

    # Sauvegarder les dossisers des eleves cherchees dans liste
    for prof in liste_des_profs:
        r = re.compile(f'.+?/{prof}/.+?EDJ/.{{1,4}}?[\w]+\s[\w]+') # I dont know ca marche 
        paths += list(filter(r.match, temp))

    # Avec les paths des élèves, créer la liste des combinaison "to" & "from"
    # Pour chaque élève,
    for path_dossier_eleve in paths:
        for path_fiche in files_to_copy:
            nom_de_la_fiche = path_fiche.split("/")[-1] # Je veux juste le nom de la fiche, "example/path/(nomDeLaFiche)"
            batch_copy_paths.append(dropbox.files.RelocationPath(from_path= path_fiche, 
                                                                to_path = path_dossier_eleve + "/" + nom_de_la_fiche))


    # Finalement, batch copie
    dbx.files_copy_batch_v2(batch_copy_paths, autorename=False)

    
# copier_fiches_profs(["Manon Banon", "Eric Something"])