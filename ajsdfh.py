import dropbox
import re
import glob
import os


path_dossier_eleve = "03 - Élèves EDJ"
token = "n28tOc_l7VQAAAAAAAAAASZTxqtgAWGts4sgwR3fowxCVzfSbRf2vrZmEHcwFOwf"

dbx = dropbox.Dropbox(token)

liste_des_profs = ["Eric Something", "Jimmy Jam"]
liste_des_eleves = []
temp = []
batch_copy_paths = []
automation = "/Automation"
files_to_copy  = []

# ~~~~~~~~~~~~Trouve le path du dossier a copier dans la machine~~~~~~~~~~``

# nom_du_dossier_a_copier = input("Nom du dossier contentant les documents a copier \n\n")

# dossierACopier = glob.glob(os.path.expanduser("~") + "/**/" + nom_du_dossier_a_copier)[0]
# print(dossierACopier)

# ~~~~~~~~~~~~ Ajouter tout les dossiers a copier ~~~~~~~~~~~~~~~~~~~~~

for file in dbx.files_list_folder(path = automation).entries:
    files_to_copy.append(file.path_display)


# Sauvegarder tout les dossiers
folders = dbx.files_list_folder(path="/003 - Professeurs", recursive=True).entries

# Ajouter tout les paths des profs dans temp
for entry in folders:
    temp.append(entry.path_display)

# Sauvegarder les dossisers des eleves cherchees dans liste
for prof in liste_des_profs:
    r = re.compile(f'.+?/{prof}/{path_dossier_eleve}/[^\d]+')
    # r = re.compile(f'.+?/{prof}/{path_dossier_eleve}/Timmy+')
    liste_des_eleves+= list(filter(r.match, temp))

# For each student, copy the files to the students main folder path
for i in liste_des_eleves:
    for j in files_to_copy:
        file_name = j.split("/")[-1]
        batch_copy_paths.append(dropbox.files.RelocationPath(from_path= j, to_path = i + "/" + file_name))



# for i in batch_copy_paths:
#     print(i)

res = dbx.files_copy_batch_v2(batch_copy_paths, autorename=True)

# jobid = res.get_async_job_id()
# print(jobid)

# while True:
#     if dbx.files_copy_batch_check_v2(jobid).is_complete():
#         break
#     sleep(10)


    # if isinstance(entry, dropbox.files.FolderMetadata):
        # liste_des_eleves.append(entry.path_display)




