from cgitb import text
import tkinter as tk
from tkinter import BOTH, END, NONE, RIGHT, Y, Label, Radiobutton, Scrollbar, StringVar, filedialog, Text
from turtle import bgcolor
import methodes_externes as me
import methodes_dropbox as md

liste_des_fiches = []
liste_des_eleves = []


def afficherFichesACopier():
    for fiche in md.fiches_a_copier_dans_dropbox():
        listbox.insert(END, fiche)

def importerDestinations():
    fiche_excel = filedialog.askopenfile(title="Choisir la fiche excel")
    # liste_des_eleves = me.extraire_eleves_excel(fiche_excel.name)
    liste_des_eleves = me.extraire_profs_excel(fiche_excel.name)
    for eleve in liste_des_eleves:
        listbox2.insert(END, eleve)

def copierFiches():
    # print(bool(radio_professeur.get()))
    if radio_professeur.get() == "professurs":
        md.copier_fiches_profs(listbox2.get(0),END)
    elif radio_eleve.get() == "élève":
        md.copier_fiches(listbox2.get(0, END))
    else:
        exit


# ~~~~~~~~~~~~~~~~ Setup dela fenêtre de l'application ~~~~~~~~~~~~~~~

# Fenêtre principale
root = tk.Tk()
root.title("Copiage des fiches automatique")
canvas = tk.Canvas(root, height=600, width=700, bg="#03AEEF")
canvas.pack()

# Section de la fenêtre contenant la liste ET les Boutons (change la valeur du paramètre "bg" à "black" pour le voir )
frame = tk.Frame(root, bg="#03AEEF")
frame.place(relwidth=0.8, relheight=0.25, relx=0.1, rely=0.15)

# Sous-section de frame pour la liste
subframeDeLaListeFiches = tk.Frame(root, bg="white")
subframeDeLaListeFiches.place(in_=frame,relwidth=0.8, relheight=0.7, relx=0.1, rely=0.05)

framePourDestination = tk.Frame(root, bg="#03AEEF") 
framePourDestination.place(relwidth=0.8, relheight=0.25, relx=0.1, rely=0.5)

# Sous-section de frame contentant la liste
subframeDeLaListeEleves = tk.Frame(root, bg="white")
subframeDeLaListeEleves.place(in_=framePourDestination,relwidth=0.8, relheight=0.75, relx=0.1, rely=0.05)

# ~~~~~~~~~~~~~~~~ Text de présentation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bienvenue = Label(root, text = "Fichiers à transférer", bg="#03AEEF").place(relx=0.4, rely=0.10)
chosirBoutons = Label(root, text = "Choisir le contenu de l'excel:", bg="#03AEEF").place(relx=0.02, rely=0.8)

# ~~~~~~~~~~~~~~~~ Création des boite de listes (Pour les fiches et noms des élèves/professeurs) ~~~~~~~~~~~~``

listbox = tk.Listbox(subframeDeLaListeFiches)
listbox.pack(fill=BOTH)
# Créer le scrollbar pour pouvoir bouger la liste verticalement
scrollbar = tk.Scrollbar(subframeDeLaListeFiches)
scrollbar.pack(side=RIGHT)
# Associer la liste et le scroll, un à l'autre
listbox.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=listbox.yview)

## Même chose qu'en haut 
listbox2 = tk.Listbox(subframeDeLaListeEleves)
listbox2.pack(fill=BOTH)
scrollbar2 = tk.Scrollbar(subframeDeLaListeEleves)
scrollbar2.pack(side=RIGHT)
listbox2.config(yscrollcommand=scrollbar.set)
scrollbar2.config(command=listbox2.yview)

# ~~~~~~~~~~~~~~~ Creation des boutons de radio ~~~~~~~~~~~~~~~

radio_eleve = StringVar(value=" ")
radio_professeur = StringVar(value= " ")

Radiobutton(root, text="Liste d'élèves", value="élève",variable=radio_eleve, bg="#03AEEF").place(relx=0.3,rely=0.8)
Radiobutton(root, text="Liste de professeurs", value="professeurs", variable=radio_professeur, bg="#03AEEF").place(relx=0.5,rely=0.8)

# ~~~~~~~~~~~~~~~~ Création des boutons (btn) ~~~~~~~~~~~~~~~~~
# Bouton pour commencer le batch copier
btnChosirExcel = tk.Button(root, text="Choisissez le fichier excel", padx=5, pady=5,fg='black', command=importerDestinations) # Add back command=importerDestinations to allow the excel import
btnChosirExcel.place(in_=framePourDestination, relx=0.32, rely=0.81 )

# Bouton pour commencer le batch copier
btnCopierFiches = tk.Button(root, text="Copier les dossiers selectionnées", padx=5, pady=5,fg='black', command=copierFiches)
btnCopierFiches.pack()

#~~~~~~~~~~~~~~~~~~ Creer la fenetre ~~~~~~~~~~~~~~~~~~~~~~~
afficherFichesACopier()
root.mainloop()
