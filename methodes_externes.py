import pandas as pd

def extraire_profs_excel(path_excel):
    df = pd.ExcelFile(path_excel).parse()
    colomns_des_profs = df.filter(like='Professeur').columns
    print(colomns_des_profs[2])
    noms_des_profs = df[colomns_des_profs[2]].drop_duplicates().dropna()
    return noms_des_profs

def extraire_eleves_excel(path_excel):
    df = pd.ExcelFile(path_excel).parse()
    noms_des_eleves = df['Nom complet'].drop_duplicates().dropna()
    return noms_des_eleves
